#include <stdio.h>
#include <mpi.h>


int main(void){
int rank;
int p;
int size=8;
int left;
int right;
char send_buffer1[8];
char recv_buffer1[8];
char send_buffer2[8];
char recv_buffer2[8];

MPI_Init(NULL,NULL);

MPI_Comm_rank(MPI_COMM_WORLD, &rank);
MPI_Comm_size(MPI_COMM_WORLD, &p);

left = (rank-1 + p) % p;
right = (rank+1) % p;

sprintf(send_buffer1, "Core %d", rank);
sprintf(send_buffer2, "Core %d", rank);

MPI_Send(send_buffer1, size, MPI_CHAR, left, 0, MPI_COMM_WORLD);
MPI_Recv(recv_buffer1, size, MPI_CHAR, right, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE);

MPI_Send(send_buffer2, size, MPI_CHAR, right, 0, MPI_COMM_WORLD);
MPI_Recv(recv_buffer2, size, MPI_CHAR, left, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE);

// Because send and receives are happening between each other they might clash with each other if a process 
// is slow to send or receive and this will create a deadlock.

printf("I am Core %d, my left neighbour is %s, my right neighbour is %s\n",rank,recv_buffer2,recv_buffer1);

MPI_Finalize();
return 0;
}

